#!/bin/bash
sudo chmod -R 0777 ./master
sudo chmod -R 0777 ./slave

docker-ip() {
    docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$@"
}

priv_stmt='GRANT REPLICATION SLAVE ON *.* TO "ikbcluster"@"%" IDENTIFIED BY "admin"; FLUSH PRIVILEGES;'
priv_mrpuser="CREATE USER 'mrpuser'@'%' IDENTIFIED BY 'admin';"

until docker exec mysql_master sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e ";"'
do
    echo "Waiting for MASTER root Cupu1234!@ database connection..."
    sleep 3
done
# docker exec mysql_master sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e "$priv_mrpuser
docker exec mysql_master sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_stmt'"

until docker exec mysql_slave sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e ";"'
do
    echo "Waiting for SLAVE root Cupu1234!@ database connection..."
    sleep 3
done
# docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e "$priv_mrpuser
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_stmt'"

MS_STATUS=`docker exec mysql_slave sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e "SHOW MASTER STATUS"'`
CURRENT_LOG=`echo $MS_STATUS | awk '{print $6}'`
CURRENT_POS=`echo $MS_STATUS | awk '{print $7}'`
start_master_slave_stmt="STOP SLAVE;CHANGE MASTER TO MASTER_HOST='$(docker-ip mysql_slave)',MASTER_USER='ikbcluster',MASTER_PASSWORD='admin',MASTER_LOG_FILE='$CURRENT_LOG',MASTER_LOG_POS=$CURRENT_POS; START SLAVE;"
start_master_slave_cmd='export MYSQL_PWD=Cupu1234!@; mysql -u root -e "'
start_master_slave_cmd+="$start_master_slave_stmt"
start_master_slave_cmd+='"'
docker exec mysql_master sh -c "$start_master_slave_cmd"

MS_STATUS=`docker exec mysql_master sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e "SHOW MASTER STATUS"'`
CURRENT_LOG=`echo $MS_STATUS | awk '{print $6}'`
CURRENT_POS=`echo $MS_STATUS | awk '{print $7}'`
start_slave_master_stmt="STOP SLAVE;CHANGE MASTER TO MASTER_HOST='$(docker-ip mysql_master)',MASTER_USER='ikbcluster',MASTER_PASSWORD='admin',MASTER_LOG_FILE='$CURRENT_LOG',MASTER_LOG_POS=$CURRENT_POS; START SLAVE;"
start_slave_master_cmd='export MYSQL_PWD=Cupu1234!@; mysql -u root -e "'
start_slave_master_cmd+="$start_slave_master_stmt"
start_slave_master_cmd+='"'
docker exec mysql_slave sh -c "$start_slave_master_cmd"

echo "Information Slave";
echo "==================";
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e 'SHOW SLAVE STATUS \G'"
