#!/bin/bash
sudo docker-compose down

rm -rf ./master/data/*
rm -rf ./master/log/*
rm -rf ./slave/data/*
rm -rf ./slave/log/*
