#!/bin/bash
sudo chmod -R 0775 ./master/*
sudo chmod -R 0775 ./slave/*
sudo chmod -R 0444 ./master/mysql/*
sudo chmod -R 0444 ./slave/mysql/*

sudo docker-compose down
sudo docker-compose build
sudo docker-compose up -d
# sudo docker-compose restart

docker-ip() {
    docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$@"
}

sleep 5
create_mrpuser='CREATE USER "mrpuser"@"%" IDENTIFIED BY "admin";'
priv_mrpuser='GRANT ALL PRIVILEGES ON *.* TO "mrpuser"@"%"; FLUSH PRIVILEGES;'
priv_stmt='GRANT REPLICATION SLAVE ON *.* TO "ikbcluster"@"%" IDENTIFIED BY "admin"; FLUSH PRIVILEGES;'

until docker exec mysql_master sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e ";"'
do
    echo "Waiting for mysql_master root Cupu1234!@ database connection..."
    sleep 3
done
docker exec mysql_master sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$create_mrpuser'"
docker exec mysql_master sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_mrpuser'"
docker exec mysql_master sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_stmt'"

until docker exec mysql_slave sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e ";"'
do
    echo "Waiting for mysql_master root Cupu1234!@ database connection..."
    sleep 3
done
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$create_mrpuser'"
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_mrpuser'"
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e '$priv_stmt'"

MS_STATUS=`docker exec mysql_slave sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e "SHOW MASTER STATUS"'`
CURRENT_LOG=`echo $MS_STATUS | awk '{print $6}'`
CURRENT_POS=`echo $MS_STATUS | awk '{print $7}'`
start_slave_master_stmt="STOP SLAVE;CHANGE MASTER TO MASTER_HOST='$(docker-ip mysql_slave)',MASTER_USER='ikbcluster',MASTER_PASSWORD='admin',MASTER_LOG_FILE='$CURRENT_LOG',MASTER_LOG_POS=$CURRENT_POS; START SLAVE;"
start_slave_master_cmd='export MYSQL_PWD=Cupu1234!@; mysql -u root -e "'
start_slave_master_cmd+="$start_slave_master_stmt"
start_slave_master_cmd+='"'
echo $start_slave_master_cmd;
docker exec mysql_master sh -c "$start_slave_master_cmd"

MS_STATUS=`docker exec mysql_master sh -c 'export MYSQL_PWD=Cupu1234!@; mysql -u root -e "SHOW MASTER STATUS"'`
CURRENT_LOG=`echo $MS_STATUS | awk '{print $6}'`
CURRENT_POS=`echo $MS_STATUS | awk '{print $7}'`
start_slave_master_stmt="STOP SLAVE;CHANGE MASTER TO MASTER_HOST='$(docker-ip mysql_master)',MASTER_USER='ikbcluster',MASTER_PASSWORD='admin',MASTER_LOG_FILE='$CURRENT_LOG',MASTER_LOG_POS=$CURRENT_POS; START SLAVE;"
start_slave_master_cmd='export MYSQL_PWD=Cupu1234!@; mysql -u root -e "'
start_slave_master_cmd+="$start_slave_master_stmt"
start_slave_master_cmd+='"'
echo $start_slave_master_cmd;
docker exec mysql_slave sh -c "$start_slave_master_cmd"

echo "Information Slave";
echo "==================";
docker exec mysql_slave sh -c "export MYSQL_PWD=Cupu1234!@; mysql -u root -e 'SHOW SLAVE STATUS \G'"
